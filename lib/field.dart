import 'package:flutter/material.dart';

import 'cagn_editor_theme.dart';
import 'cagn_editor_theme_data.dart';

class CagnField extends StatelessWidget {
  final EdgeInsets margin;
  final EdgeInsets padding;
  final Widget control;
  final String label;
  final Axis axis;

  const CagnField({
    Key? key,
    required this.control,
    required this.label,
    this.margin: EdgeInsets.zero,
    this.padding: EdgeInsets.zero,
    this.axis: Axis.vertical,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    CagnEditorThemeData themeData = CagnEditorTheme.of(context);
    TextStyle? labelStyle = themeData.textStyle != null
        ? themeData.textStyle!
            .copyWith(fontSize: Theme.of(context).textTheme.caption!.fontSize)
        : Theme.of(context).textTheme.caption;
    final labelControl = Text(label,
        style: labelStyle!.copyWith(
            color: themeData.colorScheme!.onBackground.withOpacity(0.6)));
    return Container(
      padding: padding,
      margin: margin,
      child: axis == Axis.vertical
          ? Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                labelControl,
                SizedBox(height: 2),
                control,
              ],
            )
          : Row(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                labelControl,
                SizedBox(width: 10),
                Expanded(child: control),
              ],
            ),
    );
  }
}
