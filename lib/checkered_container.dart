import 'package:flutter/material.dart';

enum CheckerSize {
  small,
  medium,
  large,
}

class CheckeredContainer extends StatelessWidget {
  final double height;
  final double width;
  final double borderRadius;
  final CheckerSize size;
  final Widget child;

  const CheckeredContainer({
    Key? key,
    required this.child,
    this.height: 10,
    this.width: 10,
    this.size: CheckerSize.small,
    this.borderRadius: 0,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(borderRadius),
      child: Container(
        height: height,
        width: width,
        decoration:
            BoxDecoration(borderRadius: BorderRadius.circular(borderRadius)),
        child: Stack(
          children: [
            Positioned(
              left: 0,
              top: 0,
              width: _adjustedWidth,
              height: _adjustedHeight,
              child: _background,
            ),
            Positioned.fill(child: child),
          ],
        ),
      ),
    );
  }

  Widget get _background {
    List<Widget> checkers = [];
    // print(_checkerCount);
    for (int i = 0; i < _checkerCount; i++) {
      checkers.add(Container(
        color: i % 2 == 0 ? Colors.grey.shade400 : Colors.white,
      ));
    }
    return GridView.count(
      crossAxisCount: _axisCount,
      physics: NeverScrollableScrollPhysics(),
      children: checkers,
    );
  }

  int get _checkerCount => _axisCount * _heightCount;

  int get _checkerSize => size == CheckerSize.small
      ? 5
      : size == CheckerSize.medium
          ? 10
          : 20;

  int get _axisCount => _widthCount % 2 == 0 ? _widthCount - 1 : _widthCount;

  int get _widthCount => _adjustedWidth ~/ _checkerSize;

  int get _heightCount => _adjustedHeight ~/ _checkerSize;

  double get _adjustedHeight => height > width ? height * 1.3 : height * 1.5;

  double get _adjustedWidth => height > width ? width * 1.3 : width;
}
