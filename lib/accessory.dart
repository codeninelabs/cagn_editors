import 'package:cagn_popup/cagn_popup.dart';
import 'package:flutter/material.dart';

import 'cagn_editor_theme_data.dart';

class Accessory extends StatelessWidget {
  final Widget? dialogWidget;
  final Alignment dialogAlignment;
  final Alignment dialogPivot;
  final Size dialogSize;
  final Offset dialogOffset;
  final bool disabled;
  final bool noDivider;
  final IconData icon;
  final CagnEditorThemeData themeData;
  final Function(PositionDetails)? onShowDialog;
  final Function? onClose;
  final Function? onTap;
  final Function? onLongPress;
  final void Function()? onLongPressUp;

  const Accessory({
    Key? key,
    required this.icon,
    required this.themeData,
    this.dialogAlignment: Alignment.bottomRight,
    this.dialogPivot: Alignment.topRight,
    this.dialogOffset: Offset.zero,
    this.dialogSize: const Size(200, 200),
    this.dialogWidget,
    this.onShowDialog,
    this.onClose,
    this.onTap,
    this.onLongPress,
    this.onLongPressUp,
    this.disabled: false,
    this.noDivider: false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return PositionAware(
      onTap: (details) async {
        if (dialogWidget != null) {
          if (onShowDialog != null) onShowDialog!(details);
          await showCagnPopup(context,
              details: details,
              props: PopupProps(
                alignment: dialogAlignment,
                pivot: dialogPivot,
                size: dialogSize,
                offset: dialogOffset,
              ),
              child: Material(
                color: Colors.transparent,
                child: Container(
                  decoration: BoxDecoration(
                    color: themeData.colorScheme!.background,
                    borderRadius:
                        BorderRadius.circular(themeData.borderRadius!),
                  ),
                  height: dialogSize.height,
                  width: dialogSize.width,
                  child: dialogWidget,
                ),
              ));
          if (onClose != null) onClose!();
        } else {
          if (onTap != null && !disabled) onTap!();
        }
      },
      onLongPress: (_) {
        if (onLongPress != null && !disabled) onLongPress!();
      },
      onLongPressUp: (_) {
        if (onLongPressUp != null && !disabled) onLongPressUp!();
      },
      child: Container(
        color: themeData.colorScheme!.surface,
        height: themeData.height,
        padding: EdgeInsets.symmetric(horizontal: 6),
        child: Icon(
          icon,
          size: themeData.iconTheme!.size,
          color: themeData.colorScheme!.onSurface
              .withOpacity(disabled ? 0.5 : 1.0),
        ),
      ),
    );
  }
}
