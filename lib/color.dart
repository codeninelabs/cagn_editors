import 'package:cagn_core/core.dart';
import 'package:cagn_ui/widgets/widgets.dart';
import 'package:flutter/material.dart';

import 'accessory.dart';
import 'base/control.dart';
import 'cagn_editor_theme.dart';
import 'cagn_editor_theme_data.dart';

class CagnColor extends StatefulWidget {
  final Color? value;
  final EdgeInsetsGeometry? padding;
  final bool disabled;
  final bool readonly;
  final bool allowDialog;
  final Widget? dialogWidget;
  final CagnEditorThemeData? themeOverride;
  final ValueChanged<Color>? onChange;
  final Function? onDialogClose;
  final Widget? Function(Color?)? builder;

  const CagnColor({
    Key? key,
    this.value,
    this.padding,
    this.disabled: false,
    this.readonly: false,
    this.allowDialog: true,
    this.themeOverride,
    this.dialogWidget,
    this.onChange,
    this.onDialogClose,
    this.builder,
  }) : super(key: key);

  @override
  _CagnColorState createState() => _CagnColorState();
}

class _CagnColorState extends State<CagnColor> {
  Color? _value;
  Color? _opaqueColor;

  @override
  void initState() {
    super.initState();
    _value = widget.value;
    if (widget.value != null && widget.value!.opacity != 0) {
      _opaqueColor = widget.value;
    }
  }

  @override
  void didUpdateWidget(CagnColor oldWidget) {
    if (widget.value != oldWidget.value) {
      _value = widget.value;
      if (widget.value!.opacity != 0) {
        _opaqueColor = widget.value;
      }
    }
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    List<Accessory> _trailing = [];
    if (widget.allowDialog) {
      _trailing.add(Accessory(
        icon: Icons.colorize,
        themeData: themeData,
        dialogPivot: Alignment.topRight,
        dialogAlignment: Alignment.bottomRight,
        dialogOffset: Offset(0, 2),
        dialogWidget: widget.dialogWidget ??
            HexPad(
              value: _opaqueColor == null
                  ? ''
                  : ColorFactory.hexValue(_opaqueColor!),
              onEnter: (v) {
                _updateValue(ColorFactory.hexToColor(v));
                Navigator.pop(context);
              },
              containerWidth: 200,
            ),
        onClose: widget.onDialogClose,
      ));
    }
    return CagnControl<Color>(
      value: _value,
      builder: (_, Color? color) {
        if (widget.builder != null) return widget.builder!(color);
        return null;
      },
      themeOverride: widget.themeOverride,
      disabled: widget.disabled,
      trailing: _trailing,
      onTap: _handleTap,
    );
  }

  bool get _isClear => _value != null && _value!.opacity == 0;

  CagnEditorThemeData get themeData => CagnEditorTheme.of(context)
      .merge(widget.themeOverride ?? CagnEditorTheme.of(context));

  void _handleTap(details) {
    if (_isClear) return;
  }

  void _updateValue(Color v) {
    if (widget.onChange != null) widget.onChange!(v);
  }
}
