import 'package:cagn_popup/cagn_popup.dart';
import 'package:flutter/cupertino.dart' hide IconData;
import 'package:flutter/material.dart' hide IconData;

import '../accessory.dart';
import '../cagn_editor_theme_data.dart';
import 'base.dart';

class CagnControl<T> extends CagnBase {
  final T? value;
  final EdgeInsetsGeometry padding;
  final bool disabled;
  final bool fitted;
  final List<Accessory> leading;
  final List<Accessory> trailing;
  final Widget? Function(BuildContext, T?)? builder;
  final Function(PositionDetails)? onTap;
  final ValueChanged<T>? onChange;
  final CagnEditorThemeData? themeOverride;

  const CagnControl({
    Key? key,
    required this.value,
    this.disabled: false,
    this.fitted: false,
    this.padding: const EdgeInsets.all(2),
    this.leading: const [],
    this.trailing: const [],
    this.onChange,
    this.onTap,
    this.builder,
    this.themeOverride,
  }) : super(
          key: key,
          padding: padding,
          disabled: disabled,
          themeOverride: themeOverride,
        );

  @override
  _CagnControlState<T> createState() => _CagnControlState<T>();
}

class _CagnControlState<T> extends CagnBaseState<CagnControl<T>> {
  T? _value;

  @override
  void initState() {
    _value = widget.value;
    super.initState();
  }

  @override
  void didUpdateWidget(CagnControl<T> oldWidget) {
    if (widget.value != oldWidget.value) {
      _value = widget.value;
    }
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    var _c = Stack(
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            ...makeLeading,
            Expanded(
              child: widget.onTap != null
                  ? PositionAware(
                      child: _control,
                      onTap: widget.onTap,
                    )
                  : _control,
            ),
            ...makeTrailing,
          ],
        ),
        widget.disabled ? overlay : emptyContainer
      ],
    );
    return widget.fitted
        ? fitted(_c, zeroPad: true)
        : unfitted(_c, zeroPad: true);
  }

  get makeLeading => widget.leading
      .map((e) => Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: e.noDivider ? [e] : [e, accessoryDivider()],
          ))
      .toList();

  get makeTrailing => widget.trailing
      .map((e) => Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: e.noDivider ? [e] : [accessoryDivider(), e],
          ))
      .toList();

  Widget get _control {
    Widget _c = Text(_value != null ? _value!.toString() : '');
    if (widget.builder != null) {
      return widget.builder!(context, _value) ?? _c;
    }
    return _c;
  }
}
