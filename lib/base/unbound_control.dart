import 'package:cagn_popup/cagn_popup.dart';
import 'package:flutter/cupertino.dart' hide IconData;
import 'package:flutter/material.dart' hide IconData;

import '../cagn_editor_theme_data.dart';
import 'base.dart';

class CagnUnboundControl<T> extends CagnBase {
  final T? value;
  final EdgeInsetsGeometry padding;
  final bool disabled;
  final double height;
  final Widget? Function(BuildContext, T?)? builder;
  final Function(PositionDetails)? onTap;
  final ValueChanged<T>? onChange;
  final CagnEditorThemeData? themeOverride;

  const CagnUnboundControl({
    Key? key,
    required this.value,
    this.disabled: false,
    this.padding: const EdgeInsets.all(2),
    this.height: 100,
    this.onChange,
    this.onTap,
    this.builder,
    this.themeOverride,
  }) : super(
          key: key,
          padding: padding,
          disabled: disabled,
          themeOverride: themeOverride,
        );

  @override
  _CagnControlState<T> createState() => _CagnControlState<T>();
}

class _CagnControlState<T> extends CagnBaseState<CagnUnboundControl<T>> {
  T? _value;

  @override
  void initState() {
    _value = widget.value;
    super.initState();
  }

  @override
  void didUpdateWidget(CagnUnboundControl<T> oldWidget) {
    if (widget.value != oldWidget.value) {
      _value = widget.value;
    }
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    var _c = Stack(
      children: [
        Container(
          child: _control,
        ),
        widget.disabled ? overlay : emptyContainer
      ],
    );
    return loose(_c, zeroPad: true);
  }

  Widget get _control {
    Widget _c = Text(_value != null ? _value!.toString() : '');
    if (widget.builder != null) {
      return widget.builder!(context, _value) ?? _c;
    }
    return _c;
  }
}
