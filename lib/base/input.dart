import 'package:flutter/material.dart' hide IconData;

import '../accessory.dart';
import '../cagn_editor_theme_data.dart';
import 'base.dart';

class CagnInput<T> extends CagnBase {
  final T? value;
  final EdgeInsetsGeometry padding;
  final bool disabled;
  final bool readonly;
  final bool autocorrect;
  final TextInputType keyboardType;
  final TextAlign textAlign;
  final List<Accessory> leading;
  final List<Accessory> trailing;
  final ValueChanged<String>? onChange;
  final CagnEditorThemeData? themeOverride;
  final void Function(String)? onSubmitted;

  const CagnInput({
    Key? key,
    required this.value,
    this.disabled: false,
    this.readonly: false,
    this.padding: const EdgeInsets.all(2),
    this.leading: const [],
    this.trailing: const [],
    this.autocorrect: false,
    this.keyboardType: TextInputType.text,
    this.textAlign: TextAlign.left,
    this.onChange,
    this.onSubmitted,
    this.themeOverride,
  }) : super(
          key: key,
          padding: padding,
          disabled: disabled,
          themeOverride: themeOverride,
        );

  @override
  _CagnInputState createState() => _CagnInputState();
}

class _CagnInputState<T> extends CagnBaseState<CagnInput<T>> {
  T? _value;
  late FocusNode _focusNode = FocusNode(canRequestFocus: !widget.readonly);
  late TextEditingController _controller;

  @override
  void initState() {
    _value = widget.value;
    String _text = '';
    if (_value != null) {
      _text = _value!.toString();
    }
    _controller = TextEditingController(text: _text);
    super.initState();
  }

  @override
  void didUpdateWidget(CagnInput<T> oldWidget) {
    if (widget.value != oldWidget.value) {
      _value = widget.value;
      _controller.text = _value.toString();
    }
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    return unfitted(
      Stack(
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              ...makeLeading,
              Expanded(child: _control),
              ...makeTrailing,
            ],
          ),
          widget.disabled ? overlay : emptyContainer
        ],
      ),
      zeroPad: true,
    );
  }

  FocusNode get focusNode => _focusNode;

  get makeLeading => widget.leading
      .map((e) => Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: e.noDivider ? [e] : [e, accessoryDivider()],
          ))
      .toList();

  get makeTrailing => widget.trailing
      .map((e) => Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: e.noDivider ? [e] : [accessoryDivider(), e],
          ))
      .toList();

  Widget get _control => TextField(
        readOnly: widget.readonly,
        style: themeData.inputStyle,
        autocorrect: widget.autocorrect,
        enabled: !widget.disabled,
        keyboardType: widget.keyboardType,
        textAlign: widget.textAlign,
        controller: _controller,
        focusNode: _focusNode,
        decoration: InputDecoration(
          border: InputBorder.none,
          isDense: true,
        ),
        onSubmitted: widget.onSubmitted,
        onChanged: (String v) {
          if (widget.onChange != null) widget.onChange!(v);
        },
      );
}
