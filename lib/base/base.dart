import 'package:cagn_editors/config.dart';
import 'package:flutter/material.dart';

import '../cagn_editor_theme.dart';
import '../cagn_editor_theme_data.dart';

class CagnBase extends StatefulWidget {
  final EdgeInsetsGeometry padding;
  final bool disabled;
  final CagnEditorThemeData? themeOverride;

  const CagnBase({
    Key? key,
    this.disabled: false,
    this.padding: const EdgeInsets.all(2),
    this.themeOverride,
  }) : super(key: key);

  @override
  CagnBaseState createState() => CagnBaseState();
}

class CagnBaseState<T extends CagnBase> extends State<T> {
  final GlobalKey _key = GlobalKey();
  double? _width;

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback(_initKeyData);
    super.initState();
  }

  void _initKeyData(_) {
    if (_width == null) {
      if (_key.currentContext != null) {
        final box = _key.currentContext!.findRenderObject() as RenderBox;
        setState(() {
          _width = box.size.width;
        });
      }
    }
  }

  CagnEditorThemeData get themeData => CagnEditorTheme.of(context)
      .merge(widget.themeOverride ?? CagnEditorTheme.of(context));

  double? get width => _width;

  @override
  Widget build(BuildContext context) {
    throw UnimplementedError();
  }

  Widget fitted(Widget child, {bool zeroPad = false}) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(themeData.borderRadius!),
      child: FittedBox(
        child: Container(
          key: _key,
          padding: zeroPad
              ? EdgeInsets.zero
              : EdgeInsets.symmetric(vertical: 2, horizontal: 4),
          alignment: Alignment.center,
          decoration: BoxDecoration(
            color: themeData.colorScheme!.background,
            borderRadius: BorderRadius.circular(themeData.borderRadius!),
          ),
          child: child,
        ),
      ),
    );
  }

  Widget unfitted(Widget child, {bool zeroPad = false, double? height}) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(themeData.borderRadius!),
      child: Container(
        key: _key,
        padding: zeroPad
            ? EdgeInsets.zero
            : EdgeInsets.symmetric(vertical: 2, horizontal: 4),
        alignment: Alignment.center,
        height: height ?? themeData.height,
        decoration: BoxDecoration(
          color: themeData.colorScheme!.background,
          borderRadius: BorderRadius.circular(themeData.borderRadius!),
        ),
        child: child,
      ),
    );
  }

  Widget loose(Widget child, {bool zeroPad = false}) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(themeData.borderRadius!),
      child: Container(
        key: _key,
        padding: zeroPad
            ? EdgeInsets.zero
            : EdgeInsets.symmetric(vertical: 2, horizontal: 4),
        alignment: Alignment.center,
        decoration: BoxDecoration(
          color: themeData.colorScheme!.background,
          borderRadius: BorderRadius.circular(themeData.borderRadius!),
        ),
        child: child,
      ),
    );
  }

  Widget accessoryDivider({hide: false}) {
    return VerticalDivider(
      width: hide ? 0 : 0.5,
      color: themeData.colorScheme!.onBackground.withOpacity(hide ? 0 : 0.3),
    );
  }

  Widget get emptyContainer => Container();

  Widget get overlay => Container(
        width: width ?? 0,
        height: themeData.height ?? 0,
        color: themeData.colorScheme!.background
            .withOpacity(themeData.overlayOpacity ?? CagnEditorsConfig.kDefaultOverlayOpacity),
      );
}
