import 'package:flutter/material.dart';

import 'cagn_editor_theme_data.dart';

class CagnEditorTheme extends StatelessWidget {
  const CagnEditorTheme({
    Key? key,
    required this.themeData,
    required this.child,
  }) : super(key: key);

  final CagnEditorThemeData themeData;
  final Widget child;

  static CagnEditorThemeData of(BuildContext context) {
    final _CagnEditorTheme? inheritedTheme =
        context.dependOnInheritedWidgetOfExactType<_CagnEditorTheme>();
    final CagnEditorThemeData theme =
        inheritedTheme?.theme ?? CagnEditorThemeData.fallback();
    return theme;

    // return context.dependOnInheritedWidgetOfExactType(aspect: CagnEditorTheme) as CagnEditorTheme;
  }

  @override
  Widget build(BuildContext context) {
    return _CagnEditorTheme(
      theme: themeData,
      child: child,
    );
  }
}

class _CagnEditorTheme extends InheritedTheme {
  const _CagnEditorTheme({
    Key? key,
    required this.theme,
    required Widget child,
  }) : super(key: key, child: child);

  final CagnEditorThemeData theme;

  @override
  Widget wrap(BuildContext context, Widget child) {
    return CagnEditorTheme(themeData: theme, child: child);
  }

  @override
  bool updateShouldNotify(_CagnEditorTheme old) => theme != old.theme;
}
