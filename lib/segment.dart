import 'package:flutter/cupertino.dart' hide IconData;
import 'package:flutter/widgets.dart' show IconData;

import 'base/control.dart';
import 'cagn_editor_theme.dart';
import 'cagn_editor_theme_data.dart';

class CagnSegment<T extends Object> extends StatefulWidget {
  final T? value;
  final List<T> values;
  final EdgeInsetsGeometry padding;
  final bool canDeselect;
  final bool sliding;
  final bool disabled;
  final ValueChanged<T?>? onChange;
  final CagnEditorThemeData? themeOverride;
  final Widget Function(T)? builder;

  const CagnSegment({
    Key? key,
    required this.values,
    required this.value,
    this.sliding: true,
    this.disabled: false,
    this.canDeselect: false,
    this.padding: const EdgeInsets.all(2),
    this.onChange,
    this.themeOverride,
    this.builder,
  }) : super(key: key);

  @override
  _CagnSegmentState<T> createState() => _CagnSegmentState<T>();
}

class _CagnSegmentState<T extends Object> extends State<CagnSegment<T>> {
  @override
  Widget build(BuildContext context) {
    return CagnControl<T>(
      value: widget.value,
      disabled: widget.disabled,
      themeOverride: widget.themeOverride,
      builder: (_, v) => widget.sliding
          ? FittedBox(
              child: _control,
              alignment: Alignment.center,
              fit: BoxFit.fitHeight,
            )
          : FittedBox(
              child: _controlNoSlide,
              alignment: Alignment.center,
              fit: BoxFit.fitHeight,
            ),
    );
  }

  CagnEditorThemeData get themeData => CagnEditorTheme.of(context)
      .merge(widget.themeOverride ?? CagnEditorTheme.of(context));

  Widget get _control => CupertinoSlidingSegmentedControl<T>(
        children: _segmentData,
        groupValue: widget.value,
        backgroundColor: themeData.colorScheme!.background,
        thumbColor: themeData.colorScheme!.primary
            .withOpacity(widget.disabled ? 0.8 : 1),
        onValueChanged: widget.disabled ? (_) {} : widget.onChange!,
        padding: widget.padding,
      );

  Widget get _controlNoSlide => CupertinoSegmentedControl<T>(
        children: _segmentData,
        groupValue: widget.value,
        borderColor: themeData.colorScheme!.background,
        unselectedColor: themeData.colorScheme!.background,
        selectedColor: themeData.colorScheme!.primary,
        pressedColor: themeData.colorScheme!.secondary,
        onValueChanged: widget.onChange!,
        padding: EdgeInsets.symmetric(
          vertical: widget.padding.vertical,
          horizontal: widget.padding.horizontal,
        ),
      );

  Widget _segmentWidget(T v) {
    if (widget.builder != null) return widget.builder!(v);

    if (v.runtimeType == IconData) {
      return Icon(
        v as IconData,
        size: themeData.iconTheme!.size,
        color: themeData.colorScheme!.onBackground,
      );
    }
    return Text(
      v.toString(),
      style: themeData.textStyle,
    );
  }

  Map<T, Widget> get _segmentData => widget.values
      .asMap()
      .map((key, value) => MapEntry(value, _segmentWidget(value)));
}
