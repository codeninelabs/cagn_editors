import 'package:cagn_popup/cagn_popup.dart';
import 'package:flutter/material.dart';

import 'base/control.dart';
import 'cagn_editor_theme.dart';
import 'cagn_editor_theme_data.dart';

class CagnColorHistory extends StatefulWidget {
  final Color? color;
  final double borderRadius;
  final int historySize;
  final bool disabled;
  final CagnEditorThemeData? themeOverride;
  final ScrollPhysics scrollPhysics;
  final ValueChanged<Color>? onChange;

  const CagnColorHistory({
    Key? key,
    this.historySize: 5,
    this.borderRadius: 0,
    this.disabled: false,
    this.scrollPhysics: const ClampingScrollPhysics(),
    this.themeOverride,
    this.onChange,
    this.color,
  }) : super(key: key);

  @override
  _CagnColorHistoryState createState() => _CagnColorHistoryState();
}

class _CagnColorHistoryState extends State<CagnColorHistory> {
  List<Color> _colorHistory = [];

  @override
  void initState() {
    super.initState();
    if (widget.color != null) _colorHistory.add(widget.color!);
  }

  @override
  void didUpdateWidget(CagnColorHistory oldWidget) {
    super.didUpdateWidget(oldWidget);
    if (widget.color != null && widget.color != oldWidget.color) {
      if (!_colorHistory.contains(widget.color))
        _colorHistory.add(widget.color!);
    }
  }

  @override
  Widget build(BuildContext context) {
    return CagnControl(
      value: true,
      builder: (BuildContext context, _) => _control,
      themeOverride: widget.themeOverride!.copyWith(borderRadius: 0),
      disabled: widget.disabled,
    );
  }

  Widget get _control => Container(
        padding: EdgeInsets.symmetric(horizontal: 5),
        child: SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          physics: widget.scrollPhysics,
          child: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: _colorHistory
                .map((e) => PositionAware(
                      onTap: (_) {
                        if (widget.onChange != null) widget.onChange!(e);
                      },
                      child: Container(
                        decoration: BoxDecoration(
                          color: e,
                          borderRadius:
                              BorderRadius.circular(themeData.borderRadius!),
                        ),
                        width: themeData.height! - 10,
                        height: themeData.height! - 10,
                        margin: EdgeInsets.only(right: 5),
                      ),
                    ))
                .toList(),
          ),
        ),
      );

  CagnEditorThemeData get themeData => CagnEditorTheme.of(context)
      .merge(widget.themeOverride ?? CagnEditorTheme.of(context));
}
