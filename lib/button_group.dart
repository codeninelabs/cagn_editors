import 'package:cagn_popup/cagn_popup.dart';
import 'package:flutter/material.dart' hide IconData;

import 'base/unbound_control.dart';
import 'cagn_editor_theme.dart';
import 'cagn_editor_theme_data.dart';
import 'option_item.dart';

class CagnButtonGroup<T> extends StatefulWidget {
  final T? value;
  final List<CagnOptionItem<T>> values;
  final EdgeInsetsGeometry padding;
  final AlignmentGeometry alignment;
  final bool canDeselect;
  final bool disabled;
  final double axisSpacing;
  final double size;
  final ValueChanged<T?>? onChange;
  final CagnEditorThemeData? themeOverride;
  final Widget Function(CagnOptionItem<T>)? builder;

  const CagnButtonGroup({
    Key? key,
    required this.values,
    required this.value,
    this.disabled: false,
    this.canDeselect: false,
    this.padding: const EdgeInsets.all(2),
    this.alignment: Alignment.centerLeft,
    this.axisSpacing: 5,
    this.size: 100,
    this.onChange,
    this.themeOverride,
    this.builder,
  }) : super(key: key);

  @override
  _CagnButtonGroupState<T> createState() => _CagnButtonGroupState<T>();
}

class _CagnButtonGroupState<T> extends State<CagnButtonGroup<T>> {
  @override
  Widget build(BuildContext context) {
    return CagnUnboundControl<T>(
      value: widget.value,
      disabled: widget.disabled,
      themeOverride: widget.themeOverride,
      height: widget.size,
      builder: (_, v) => _control,
    );
  }

  CagnEditorThemeData get themeData => CagnEditorTheme.of(context)
      .merge(widget.themeOverride ?? CagnEditorTheme.of(context));

  Widget get _control => Container(
        alignment: widget.alignment,
        padding: widget.padding,
        child: Wrap(
          children: _gridData,
          alignment: WrapAlignment.start,
          spacing: widget.axisSpacing,
          runSpacing: widget.axisSpacing,
        ),
      );

  Widget _gridWidget(CagnOptionItem<T> v) {
    if (widget.builder != null) return widget.builder!(v);
    Widget _w = v.icon != null
        ? Icon(
            v.icon,
            size: themeData.iconTheme!.size,
            color: widget.value == v.value
                ? themeData.colorScheme!.onPrimary
                : themeData.colorScheme!.onSurface,
          )
        : Text(
            v.label.toString(),
            style: themeData.textStyle!.copyWith(
              color: widget.value == v.value
                  ? themeData.colorScheme!.onPrimary
                  : themeData.colorScheme!.onSurface,
            ),
          );
    return PositionAware(
      onTap: (_) {
        if (widget.onChange != null) {
          if (widget.canDeselect) {
            if (widget.value != v) {
              widget.onChange!(v.value);
            } else {
              widget.onChange!(null);
            }
          } else {
            if (widget.value != v) {
              widget.onChange!(v.value);
            }
          }
        }
      },
      child: Container(
        child: _w,
        alignment: Alignment.center,
        height: widget.size,
        width: widget.size,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(themeData.borderRadius!),
          color: widget.value == v.value
              ? themeData.colorScheme!.primary
              : themeData.colorScheme!.surface,
        ),
      ),
    );
  }

  List<Widget> get _gridData => widget.values
      .map((CagnOptionItem<T> value) => _gridWidget(value))
      .toList();
}
