import 'dart:math' show min, max;

import 'package:cagn_popup/cagn_popup.dart';
import 'package:flutter/material.dart';

import 'base/base.dart';
import 'cagn_editor_theme.dart';
import 'cagn_editor_theme_data.dart';

class CagnSlider extends CagnBase {
  final double min;
  final double max;
  final double value;
  final double pointSize;
  final bool showLimits;
  final EdgeInsetsGeometry padding;
  final bool alwaysShowValue;
  final int decimalPlaces;
  final bool disabled;
  final bool showStepMarkers;
  final double? step;
  final List<double> points;
  final CagnEditorThemeData? themeOverride;
  final ValueChanged<double>? onChange;
  final Function(PositionDetails)? onTap;
  final Function(PositionDetails)? onLongPress;

  const CagnSlider({
    Key? key,
    this.min: 0,
    this.max: 10,
    this.value: 0,
    this.pointSize: 5,
    this.decimalPlaces: 0,
    this.padding: const EdgeInsets.all(2),
    this.disabled: false,
    this.showStepMarkers: false,
    this.showLimits: true,
    this.alwaysShowValue: false,
    this.points: const [],
    this.step,
    this.onChange,
    this.onTap,
    this.onLongPress,
    this.themeOverride,
  }) : super(
          key: key,
          padding: padding,
          disabled: disabled,
          themeOverride: themeOverride,
        );

  @override
  _CagnSliderState createState() => _CagnSliderState();
}

class _CagnSliderState extends CagnBaseState<CagnSlider> {
  final GlobalKey _minKey = GlobalKey();
  final GlobalKey _maxKey = GlobalKey();
  bool _dragging = false;

  int? _minX, _maxX;
  // double _value = 0;

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback(_initExtraKeyData);
  }

  void _initExtraKeyData(_) {
    if (_minX == null) {
      if (_minKey.currentContext != null && width != null) {
        final boxMin = _minKey.currentContext!.findRenderObject() as RenderBox;
        final pos = boxMin.localToGlobal(Offset.zero);
        final halfWay = (pos.dx + boxMin.size.width) / 2;
        _minX = max(((halfWay / width!) * 100).toInt(), 5);
      }
    }

    if (_maxX == null) {
      if (_maxKey.currentContext != null && width != null) {
        final boxMax = _maxKey.currentContext!.findRenderObject() as RenderBox;
        final posMax = boxMax.localToGlobal(Offset.zero);
        _maxX = ((((posMax.dx + 10) / width!) * 100).toInt());
      }
    }
    setState(() {});
  }

  @override
  void didUpdateWidget(CagnSlider oldWidget) {
    if (widget.value != oldWidget.value) {
      // _value = max(widget.min, min(widget.max, widget.value));
    }
    super.didUpdateWidget(oldWidget);
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return unfitted(
        Stack(
          children: [
            _makeBackground(),
            _makeFill(),
            _makePoints(),
            widget.showStepMarkers ? _makeStepMarkers() : Container(),
            _makeLimitLabels(),
            _makeValue(),
          ],
        ),
        zeroPad: true);
  }

  CagnEditorThemeData get themeData => CagnEditorTheme.of(context)
      .merge(widget.themeOverride ?? CagnEditorTheme.of(context));

  double get _activeWidth {
    final range = widget.max - widget.min;
    final valueRatio = (widget.value - widget.min) / range;
    return (width ?? 1) * valueRatio;
  }

  int get _percentComplete {
    return ((_activeWidth / (width ?? 1)) * 100).round();
  }

  TextStyle _labelStyle(int limit, {withOpacity: false}) {
    Color _color;
    if (withOpacity && _dragging) {
      _color = _percentComplete < limit
          ? themeData.colorScheme!.onBackground.withOpacity(0.4)
          : themeData.colorScheme!.onPrimary.withOpacity(0.4);
    } else {
      _color = _percentComplete < limit
          ? themeData.colorScheme!.onBackground
          : themeData.colorScheme!.onPrimary;
    }
    return themeData.textStyle!.copyWith(color: _color);
  }

  void _handleDragUpdate(DragUpdateDetails details) {
    _updateValue(details.localPosition.dx);
  }

  void _handleDragDown(DragDownDetails details) {
    _updateValue(details.localPosition.dx);
    setState(() {
      _dragging = true;
    });
  }

  void _handleDragEnd(DragEndDetails details) {
    setState(() {
      _dragging = false;
    });
  }

  void _handleDragCancel() {
    setState(() {
      _dragging = false;
    });
  }

  void _handleTap(details) {
    if (widget.onTap != null) widget.onTap!(details);
  }

  void _handleLongPress(details) {
    if (widget.onLongPress != null) widget.onLongPress!(details);
  }

  void _updateValue(double dx) {
    final range = widget.max - widget.min;
    final deltaRatio = dx / (width ?? 1);
    final val = range * deltaRatio + widget.min;
    if (widget.step != null) {
      var multiplier = (val / widget.step!).round();
      if (widget.onChange != null)
        widget.onChange!(
            max(widget.min, min(widget.max, widget.step! * multiplier)));
    } else {
      if (widget.onChange != null)
        widget.onChange!(max(widget.min, min(widget.max, val)));
    }
  }

  _makeBackground() {
    return Positioned.fill(
      child: widget.disabled
          ? Container(
              color: themeData.colorScheme!.background.withOpacity(0.4),
            )
          : GestureDetector(
              onHorizontalDragUpdate: _handleDragUpdate,
              onHorizontalDragDown: _handleDragDown,
              onHorizontalDragEnd: _handleDragEnd,
              onHorizontalDragCancel: _handleDragCancel,
              child: Container(
                color: themeData.colorScheme!.background,
              ),
            ),
    );
  }

  _makeFill() {
    return Positioned(
      top: 0,
      bottom: 0,
      left: 0,
      width: _activeWidth,
      child: widget.disabled
          ? Container(
              color: themeData.colorScheme!.secondary.withOpacity(0.4),
            )
          : GestureDetector(
              onHorizontalDragUpdate: _handleDragUpdate,
              onHorizontalDragDown: _handleDragDown,
              onHorizontalDragEnd: _handleDragEnd,
              onHorizontalDragCancel: _handleDragCancel,
              child: Container(
                color: themeData.colorScheme!.secondary,
              ),
            ),
    );
  }

  _makeLimitLabels() {
    return Positioned(
        top: 0,
        bottom: 0,
        left: 10,
        right: 10,
        child: widget.showLimits
            ? Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Text(
                    widget.min.toInt().toString(),
                    key: _minKey,
                    style: _labelStyle((_minX ?? 1), withOpacity: true),
                  ),
                  Text(
                    widget.max.toInt().toString(),
                    key: _maxKey,
                    style: _labelStyle((_maxX ?? 1), withOpacity: true),
                  ),
                ],
              )
            : Container());
  }

  _makeValue() {
    final noAction = widget.onTap == null && widget.onLongPress == null;
    return Positioned.fill(
      child: _dragging || widget.alwaysShowValue
          ? Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                noAction
                    ? Container(
                        padding:
                            EdgeInsets.symmetric(horizontal: 10, vertical: 2),
                        alignment: Alignment.center,
                        child: Text(
                          widget.value.toStringAsFixed(widget.decimalPlaces),
                          style: _labelStyle(50).copyWith(
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      )
                    : PositionAware(
                        onTap: widget.disabled ? null : _handleTap,
                        onLongPress: widget.disabled ? null : _handleLongPress,
                        child: Container(
                          padding:
                              EdgeInsets.symmetric(horizontal: 10, vertical: 2),
                          alignment: Alignment.center,
                          child: Text(
                            widget.value.toStringAsFixed(widget.decimalPlaces),
                            style: _labelStyle(50).copyWith(
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ),
                      ),
              ],
            )
          : Container(),
    );
  }

  _makePoints() {
    return Positioned.fill(
      child: Stack(
        children: widget.points.map((p) {
          final range = widget.max - widget.min;
          final valueRatio = (p - widget.min) / range;
          final pos = (width ?? 1) * valueRatio;

          return Positioned(
            bottom: 2,
            left: pos,
            child: _SliderPoint(
              size: widget.pointSize,
              color: pos < _activeWidth
                  ? themeData.colorScheme!.onSecondary
                  : themeData.colorScheme!.onBackground,
            ),
          );
        }).toList(),
      ),
    );
  }

  _makeStepMarkers() {
    final List<Positioned> marks = [];
    final start = widget.min + widget.step!;
    final end = widget.max - widget.step!;
    for (double p = start; p <= end; p += widget.step!) {
      final range = widget.max - widget.min;
      final valueRatio = (p - widget.min) / range;
      final pos = (width ?? 1) * valueRatio;

      marks.add(Positioned(
        top: 0,
        bottom: 0,
        left: pos,
        child: _SliderMark(
          size: widget.pointSize,
          color: pos < _activeWidth
              ? themeData.colorScheme!.onSecondary.withOpacity(0.4)
              : themeData.colorScheme!.onBackground.withOpacity(0.1),
        ),
      ));
    }
    return Positioned(
      left: 0,
      right: 0,
      top: (themeData.height! - 5) / 2,
      height: themeData.height! - 10,
      child: Stack(children: marks),
    );
  }
}

class _SliderPoint extends StatelessWidget {
  final double size;
  final Color color;

  const _SliderPoint({Key? key, this.size: 5, this.color: Colors.black})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(25),
        color: color,
      ),
      width: size,
      height: size,
    );
  }
}

class _SliderMark extends StatelessWidget {
  final double size;
  final Color color;

  const _SliderMark({Key? key, this.size: 30, this.color: Colors.black})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(25),
        color: color,
      ),
      width: 2,
      height: size,
    );
  }
}
