import 'package:flutter/widgets.dart';

class CagnOptionItem<T> {
  final T? value;
  final String label;
  final IconData? icon;
  final String? note;

  const CagnOptionItem({
    this.label: '',
    this.value,
    this.note,
    this.icon,
  });
}
