import 'dart:async';

import 'package:cagn_popup/cagn_popup.dart';
import 'package:cagn_ui/cagn_ui.dart';
import 'package:flutter/material.dart' hide IconData;
import 'package:flutter/widgets.dart' show IconData;

import 'accessory.dart';
import 'base/input.dart';
import 'cagn_editor_theme.dart';
import 'cagn_editor_theme_data.dart';
import 'config.dart';

class CagnNumber extends StatefulWidget {
  final double? value;
  final EdgeInsetsGeometry padding;
  final bool disabled;
  final bool readonly;
  final bool controls;
  final bool allowDialog;
  final bool strictStep;
  final int decimalPlaces;
  final double? step;
  final double min;
  final double max;
  final Axis axis;
  final Alignment dialogAlignment;
  final Alignment dialogPivot;
  final Size dialogSize;
  final Offset dialogOffset;
  final IconData? plusIcon;
  final IconData? minusIcon;
  final IconData? dialogIcon;
  final CagnNumberType type;
  final ValueChanged<double>? onChange;
  final CagnEditorThemeData? themeOverride;
  final Function(PositionDetails?)? onTap;

  const CagnNumber({
    Key? key,
    this.value,
    this.disabled: false,
    this.readonly: true,
    this.controls: true,
    this.allowDialog: true,
    this.strictStep: false,
    this.padding: CagnEditorsConfig.kDefaultPadding,
    this.decimalPlaces: 2,
    this.step,
    this.min: double.negativeInfinity,
    this.max: double.infinity,
    this.axis: Axis.horizontal,
    this.plusIcon,
    this.minusIcon,
    this.dialogIcon,
    this.type: CagnNumberType.Double,
    this.dialogAlignment: kDefaultDialogAlignment,
    this.dialogPivot: kDefaultDialogPivot,
    this.dialogSize: kDefaultDialogSize,
    this.dialogOffset: kDefaultDialogOffset,
    this.onChange,
    this.onTap,
    this.themeOverride,
  }) : super(key: key);

  @override
  _CagnNumberState createState() => _CagnNumberState();
}

class _CagnNumberState extends State<CagnNumber> {
  double? _value;
  String _displayValue = '';
  Timer? plusTimer;
  Timer? minusTimer;

  @override
  void initState() {
    _value = _getValue(widget.value);
    if (_value != null) {
      if (widget.type == CagnNumberType.Integer) {
        _displayValue = _value!.toInt().toString();
      } else {
        _displayValue = _value!.toStringAsFixed(widget.decimalPlaces);
      }
    }
    super.initState();
  }

  @override
  void didUpdateWidget(CagnNumber oldWidget) {
    if (widget.value != oldWidget.value) {
      _value = _getValue(widget.value);
      _displayValue = widget.type == CagnNumberType.Integer
          ? _value!.toInt().toString()
          : _value!.toStringAsFixed(widget.decimalPlaces);
    }
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    List<Accessory> _trailing = [];
    List<Accessory> _leading = [];
    if (widget.controls) {
      _leading.add(Accessory(
        icon: widget.plusIcon ?? Icons.add,
        themeData: themeData,
        onTap: _handlePlusPress,
        onLongPress: _handlePlusLongPress,
        onLongPressUp: _handlePlusLongPressUp,
        disabled: widget.disabled,
      ));
      _trailing.add(Accessory(
        icon: widget.minusIcon ?? Icons.remove,
        themeData: themeData,
        onTap: _handleMinusPress,
        onLongPress: _handleMinusLongPress,
        onLongPressUp: _handleMinusLongPressUp,
        disabled: widget.disabled,
      ));
    }
    if (widget.allowDialog) {
      _trailing.add(Accessory(
        icon: widget.dialogIcon ?? Icons.calculate_outlined,
        themeData: themeData,
        dialogPivot: widget.dialogPivot,
        dialogAlignment: widget.dialogAlignment,
        dialogOffset: widget.dialogOffset,
        dialogSize: widget.dialogSize,
        dialogWidget: NumberPad(
          value: _value!,
          decimalPlaces:
              widget.type == CagnNumberType.Integer ? 0 : widget.decimalPlaces,
          containerWidth: widget.dialogSize.width,
          borderRadius: themeData.borderRadius!,
          onEnter: (v) {
            //TODO: use logic from slider to properly set this value if strict
            setValue(double.parse(v));
            Navigator.pop(context);
          },
        ),
      ));
    }
    return CagnInput<String>(
      value: _displayValue,
      themeOverride: widget.themeOverride,
      disabled: widget.disabled,
      readonly: widget.readonly,
      autocorrect: false,
      keyboardType: TextInputType.number,
      textAlign: TextAlign.center,
      trailing: _trailing,
      leading: _leading,
    );
  }

  int get _delay =>
      themeData.longPressRepeatDelay ?? CagnEditorsConfig.kLongPressRepeatDelay;

  CagnEditorThemeData get themeData => CagnEditorTheme.of(context)
      .merge(widget.themeOverride ?? CagnEditorTheme.of(context));

  double get stepValue =>
      widget.step ?? (widget.type == CagnNumberType.Integer ? 1.0 : 0.1);

  void _handleMinusPress() {
    late double value;
    if (_value == null) {
      if (widget.min == double.negativeInfinity) {
        value = 0;
      } else {
        value = widget.min;
      }
    } else {
      if (widget.strictStep && _value! - stepValue < widget.min) {
        value = _value!;
      } else {
        value = _value! - stepValue;
      }
    }
    setValue(value);
  }

  void _handleMinusLongPress() {
    minusTimer = Timer.periodic(
      Duration(milliseconds: _delay),
      (t) => _handleMinusPress(),
    );
  }

  void _handleMinusLongPressUp() {
    if (minusTimer != null) {
      minusTimer!.cancel();
      minusTimer = null;
    }
  }

  void _handlePlusPress() {
    late double value;
    if (_value == null) {
      if (widget.max == double.infinity) {
        value = 0;
      } else {
        value = widget.max;
      }
    } else {
      if (widget.strictStep && _value! + stepValue > widget.max) {
        value = _value!;
      } else {
        value = _value! + stepValue;
      }
    }
    setValue(value);
  }

  void _handlePlusLongPress() {
    plusTimer = Timer.periodic(
      Duration(milliseconds: _delay),
      (t) => _handlePlusPress(),
    );
  }

  void _handlePlusLongPressUp() {
    if (plusTimer != null) {
      plusTimer!.cancel();
      plusTimer = null;
    }
  }

  void setValue(value) {
    print(value);
    setState(() {
      _value = _getValue(value);
      if (_value == null) {
        _displayValue = '';
      } else {
        _displayValue = widget.type == CagnNumberType.Integer
            ? _value!.toInt().toString()
            : _value!.toStringAsFixed(widget.decimalPlaces);
      }
    });
    if (widget.onChange != null) widget.onChange!(_value!);
  }

  double? _getValue(value) {
    if (value == null) return null;

    if (value <= widget.max && value >= widget.min) {
      return value;
    } else if (value > widget.max) {
      if (widget.strictStep && widget.step != null) {
        return widget.max - widget.step!;
      } else {
        return widget.max;
      }
    } else if (value < widget.min) {
      if (widget.strictStep && widget.step != null) {
        return widget.min + widget.step!;
      } else {
        return widget.min;
      }
    }
    return null;
  }
}

enum CagnNumberType {
  Double,
  Integer,
}
