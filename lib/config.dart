import 'package:flutter/cupertino.dart';

class CagnEditorsConfig {
  static const double kDefaultHeight = 30.0;
  static const double kDefaultOverlayOpacity = 0.6;
  static const int kLongPressRepeatDelay = 50;
  static const EdgeInsets kDefaultPadding = const EdgeInsets.all(2);
}