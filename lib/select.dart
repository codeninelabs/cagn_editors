import 'package:cagn_editors/config.dart';
import 'package:cagn_popup/cagn_popup.dart';
import 'package:flutter/material.dart' hide IconData;
import 'package:flutter/widgets.dart' show IconData;

import 'accessory.dart';
import 'base/control.dart';
import 'cagn_editor_theme.dart';
import 'cagn_editor_theme_data.dart';
import 'option_item.dart';

class CagnSelect<T> extends StatefulWidget {
  final T? value;
  final List<CagnOptionItem<T>> values;
  final EdgeInsetsGeometry padding;
  final bool closeOnSelect;
  final bool hasClearButton;
  final bool disabled;
  final bool controls;
  final Widget notSelectedWidget;
  final IconData? clearIcon;
  final IconData? dialogIcon;
  final Size dialogSize;
  final Alignment dialogAlignment;
  final Alignment dialogPivot;
  final Offset dialogOffset;
  final CagnEditorThemeData? themeOverride;
  final ValueChanged<T?>? onChange;
  final Function? onClear;
  final Widget Function(BuildContext, CagnOptionItem<T>, T?)? listItemBuilder;
  final Widget Function(CagnOptionItem<T>)? builder;

  const CagnSelect({
    Key? key,
    required this.values,
    this.value,
    this.padding: CagnEditorsConfig.kDefaultPadding,
    this.dialogSize: kDefaultDialogSize,
    this.dialogAlignment: kDefaultDialogAlignment,
    this.dialogPivot: kDefaultDialogPivot,
    this.dialogOffset: kDefaultDialogOffset,
    this.hasClearButton: true,
    this.closeOnSelect: true,
    this.controls: true,
    this.disabled: false,
    this.notSelectedWidget: const Text(''),
    this.clearIcon,
    this.dialogIcon,
    this.listItemBuilder,
    this.builder,
    this.onChange,
    this.onClear,
    this.themeOverride,
  }) : super(key: key);

  @override
  _CagnListState<T> createState() => _CagnListState<T>();
}

class _CagnListState<T> extends State<CagnSelect<T>> {
  T? _value;

  @override
  void initState() {
    _value = widget.value;
    super.initState();
  }

  @override
  void didUpdateWidget(CagnSelect<T> oldWidget) {
    if (widget.value != oldWidget.value) {
      _value = widget.value;
    }
    super.didUpdateWidget(oldWidget);
  }

  @override
  Widget build(BuildContext context) {
    List<Accessory> _trailing = [];
    final bool canClear = widget.value != null && widget.hasClearButton;
    if (canClear) {
      _trailing.add(Accessory(
        icon: widget.clearIcon ?? Icons.clear,
        disabled: widget.disabled,
        themeData: themeData.copyWith(
          colorScheme: themeData.colorScheme!.copyWith(
            surface: themeData.colorScheme!.background,
            onSurface: themeData.colorScheme!.onBackground,
          ),
        ),
        dialogPivot: widget.dialogPivot,
        dialogAlignment: widget.dialogAlignment,
        dialogOffset: widget.dialogOffset,
        dialogSize: widget.dialogSize,
        onTap: _handleClear,
        noDivider: true,
      ));
    }
    if (widget.controls) {
      _trailing.add(Accessory(
        icon: widget.dialogIcon ?? Icons.arrow_drop_down,
        disabled: widget.disabled,
        themeData: themeData,
        dialogPivot: widget.dialogPivot,
        dialogAlignment: widget.dialogAlignment,
        dialogOffset: widget.dialogOffset,
        dialogSize: widget.dialogSize,
        dialogWidget: _listViewer,
      ));
    }

    return CagnControl<T>(
      value: _value,
      onTap: widget.controls ? null : _handleTap,
      disabled: widget.disabled,
      themeOverride: widget.themeOverride,
      trailing: _trailing,
      builder: _itemBuilder,
    );
  }

  CagnEditorThemeData get themeData => CagnEditorTheme.of(context)
      .merge(widget.themeOverride ?? CagnEditorTheme.of(context));

  void _handleTap(PositionDetails details) {
    showCagnPopup(
      context,
      details: details,
      props: PopupProps(
        alignment: widget.dialogAlignment,
        pivot: widget.dialogPivot,
        size: widget.dialogSize,
        offset: widget.dialogOffset,
      ),
      child: _listViewer,
    );
  }

  _ListViewer<T> get _listViewer => _ListViewer(
        backgroundColor: themeData.colorScheme!.background,
        color: themeData.colorScheme!.onBackground,
        highlight: themeData.colorScheme!.secondary,
        radius: themeData.borderRadius!,
        size: widget.dialogSize,
        values: widget.values,
        value: widget.value,
        listItemBuilder: widget.listItemBuilder,
        message: 'Not Found',
        onTap: (CagnOptionItem<T> item) {
          widget.onChange!(item.value);
          if (widget.closeOnSelect) Navigator.pop(context);
        },
      );

  Widget? _itemBuilder(BuildContext context, T? v) {
    if (v == null) return widget.notSelectedWidget;

    CagnOptionItem<T> _current = widget.values.singleWhere(
        (li) => li.value == v,
        orElse: () => CagnOptionItem(value: v, label: 'Not Found'));

    return widget.builder == null
        ? Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0),
            child: Text(
              _current.label,
              style: TextStyle(
                color: themeData.colorScheme!.onBackground,
              ),
              overflow: TextOverflow.ellipsis,
            ),
          )
        : widget.builder!(_current);
  }

  _handleClear() {
    if (widget.onClear != null) widget.onClear!();
  }
}

class _ListViewer<T> extends StatefulWidget {
  final Color? backgroundColor;
  final Color? color;
  final Color? highlight;
  final Size size;
  final double radius;
  final List<CagnOptionItem<T>> values;
  final T? value;
  final String message;
  final Widget Function(BuildContext, CagnOptionItem<T>, T?)? listItemBuilder;
  final void Function(CagnOptionItem<T>)? onTap;

  const _ListViewer({
    Key? key,
    this.backgroundColor,
    this.color,
    this.highlight,
    this.radius: 0,
    this.size: const Size(100, 100),
    this.values: const [],
    this.value,
    this.listItemBuilder,
    this.message: '',
    this.onTap,
  }) : super(key: key);

  @override
  __ListViewerState<T> createState() => __ListViewerState<T>();
}

class __ListViewerState<T> extends State<_ListViewer<T>> {
  T? _value;

  @override
  void initState() {
    _value = widget.value;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: Container(
        decoration: BoxDecoration(
          color: widget.backgroundColor,
          borderRadius: BorderRadius.circular(widget.radius),
        ),
        height: widget.size.height,
        width: widget.size.width,
        child: ListView(
          padding: EdgeInsets.zero,
          children: _listItems,
          controller: ScrollController(
            initialScrollOffset: _listOffset,
            keepScrollOffset: true,
          ),
        ),
      ),
    );
  }

  List<Widget> get _listItems {
    return widget.values
        .map((item) => _listItemBuilder(context, item))
        .toList();
  }

  CagnOptionItem<T> get _currentItem {
    return widget.values.singleWhere((li) => li.value == _value,
        orElse: () => CagnOptionItem(value: _value, label: widget.message));
  }

  double get _listOffset {
    return widget.values.indexOf(_currentItem) * 40;
  }

  Widget _listItemBuilder(context, CagnOptionItem<T> item) {
    return GestureDetector(
      onTap: () {
        setState(() {
          _value = item.value;
        });
        if (widget.onTap != null) widget.onTap!(item);
      },
      child: widget.listItemBuilder != null
          ? widget.listItemBuilder!(context, item, _value)
          : ListTile(
              horizontalTitleGap: 0,
              title: Text(
                item.label,
                style: TextStyle(color: widget.color),
              ),
              leading: item.icon == null
                  ? null
                  : Icon(item.icon, color: widget.color),
              trailing: Icon(
                Icons.check,
                color: item.value == _value
                    ? widget.highlight
                    : Colors.transparent,
              ),
              dense: true,
            ),
    );
  }
}
