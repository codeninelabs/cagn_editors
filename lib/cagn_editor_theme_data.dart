import 'package:cagn_ui/config.dart';
import 'package:flutter/material.dart';

import 'config.dart';

/// Defines the appearance of the editor widgets.
///
/// Used to control the appearance of the editor widgets
class CagnEditorThemeData {
  /// The [ColorScheme] for the widget.
  final ColorScheme? colorScheme;

  /// Widget height.
  final double? height;

  /// Disabled overlay opacity.
  final double? overlayOpacity;

  /// Widget border radius.
  final double? borderRadius;

  /// Widget border radius.
  final int? longPressRepeatDelay;

  /// The default appearance theme for icons.
  final IconThemeData? iconTheme;

  /// The text style for the widgets.
  final TextStyle? textStyle;

  /// The text style for the input widgets.
  final TextStyle? inputStyle;

  /// Whether widget should appear compressed.
  final bool? dense;

  /// The text overflow for the widgets.
  /// If this property is null then [softWrap] is true;
  final TextOverflow? textOverflow;

  const CagnEditorThemeData({
    this.colorScheme,
    this.iconTheme,
    this.textStyle,
    this.inputStyle,
    this.height,
    this.borderRadius,
    this.longPressRepeatDelay,
    this.dense,
    this.textOverflow,
    this.overlayOpacity,
  });

  /// Creates a [TreeView] theme with some reasonable default values.
  ///
  /// The [colorScheme] is [ColorScheme.light],
  /// the [iconTheme] is [IconThemeData.fallback],
  /// the [textStyle] is the default [TextStyle],
  /// the [borderRadius] is the default 10,
  /// and the default [height] is 30.0.
  const CagnEditorThemeData.fallback()
      : colorScheme = const ColorScheme.light(),
        iconTheme = const IconThemeData.fallback(),
        textStyle = const TextStyle(),
        inputStyle = const TextStyle(),
        textOverflow = null,
        borderRadius = CagnUIConfig.DIALOG_CORNER_RADIUS,
        longPressRepeatDelay = CagnEditorsConfig.kLongPressRepeatDelay,
        height = CagnEditorsConfig.kDefaultHeight,
        overlayOpacity = CagnEditorsConfig.kDefaultOverlayOpacity,
        dense = false;

  /// Creates a copy of this theme but with the given fields replaced with
  /// the new values.
  CagnEditorThemeData copyWith({
    ColorScheme? colorScheme,
    IconThemeData? iconTheme,
    TextStyle? textStyle,
    TextStyle? inputStyle,
    TextOverflow? textOverflow,
    bool? dense,
    double? height,
    double? borderRadius,
    double? overlayOpacity,
    int? longPressRepeatDelay,
  }) {
    return CagnEditorThemeData(
      colorScheme: colorScheme ?? this.colorScheme,
      iconTheme: iconTheme ?? this.iconTheme,
      textStyle: textStyle ?? this.textStyle,
      inputStyle: inputStyle ?? this.inputStyle,
      dense: dense ?? this.dense,
      textOverflow: textOverflow ?? this.textOverflow,
      height: height ?? this.height,
      overlayOpacity: overlayOpacity ?? this.overlayOpacity,
      borderRadius: borderRadius ?? this.borderRadius,
      longPressRepeatDelay: longPressRepeatDelay ?? this.longPressRepeatDelay,
    );
  }

  /// Returns a new theme that matches this theme but with some values
  /// replaced by the non-null parameters of the given icon theme. If the given
  /// [CagnEditorThemeData] is null, simply returns this theme.
  CagnEditorThemeData merge(CagnEditorThemeData other) {
    return copyWith(
      colorScheme: other.colorScheme,
      iconTheme: other.iconTheme,
      textStyle: other.textStyle,
      inputStyle: other.inputStyle,
      dense: other.dense,
      textOverflow: other.textOverflow,
      height: other.height,
      overlayOpacity: other.overlayOpacity,
      borderRadius: other.borderRadius,
      longPressRepeatDelay: other.longPressRepeatDelay,
    );
  }

  CagnEditorThemeData resolve(BuildContext context) => this;

  @override
  int get hashCode {
    return hashValues(
      colorScheme,
      iconTheme,
      textStyle,
      inputStyle,
      dense,
      textOverflow,
      height,
      overlayOpacity,
      borderRadius,
      longPressRepeatDelay,
    );
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;
    if (other.runtimeType != runtimeType) return false;
    return other is CagnEditorThemeData &&
        other.colorScheme == colorScheme &&
        other.iconTheme == iconTheme &&
        other.textStyle == textStyle &&
        other.inputStyle == inputStyle &&
        other.dense == dense &&
        other.textOverflow == textOverflow &&
        other.height == height &&
        other.overlayOpacity == overlayOpacity &&
        other.longPressRepeatDelay == longPressRepeatDelay &&
        other.borderRadius == borderRadius;
  }
}
